# number-analyzer

This program is dedicated to reading integer numbers which were written using special characters.  

## Prerequirements

* Visual Studio 2019 Version 16.4.2 (or later)
* .NET Core SDK 3.1

## How To Run

* Open solution in Visual Studio 2019.
* Use your Developer Comand Propmpt to add the file path into the project secret storage.  
Run ``dotnet user-secrets set "FilePath" "your filepath"``command from your ConsoleApp project route folder (not solution folder).  
If the secret manager was not initialized in your environment, run ``dotnet user-secrets init`` and try again.
* Set NumberAnalyzer.ConsoleApp project as Startup Project and build the project.
* Run the application. (or simply use ``dotner run`` command)
