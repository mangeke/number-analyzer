﻿using FluentAssertions;
using Moq;
using NumberAnalyzer.BusinessLogic.Abstract;
using NumberAnalyzer.BusinessLogic.Concrete;
using System.Linq;
using Xunit;

namespace NumberAnalyzer.Tests
{
    public class NumberParserTests
    {
        [Fact]
        public void WhenPatternExists_ShouldReturnTrueWithRightNumbers()
        {
            //Arrange
            var patternMatcherMock = new Mock<IPatternMatcher>();
            patternMatcherMock.Setup(r => r.TryReadNumber(It.IsAny<string>()))
                .Returns((string r) => (r == "test" ? (true, 5) : (false, 0)));

            var sut = new NumberParser(patternMatcherMock.Object);

            //Act
            var result = sut.ParseLine("test test\t test test");
            //Assert
            result.Should()
                .NotBeNull();

            result.allMatched.Should().BeTrue();
            result.numbers.Count().Should().Be(4);
            result.numbers.Count(r => r == 5).Should().Be(4);
        }

        [Fact]
        public void WhenPatternDoesntExist_ShouldReturnFalseWithEmptyList()
        {
            //Arrange
            var patternMatcherMock = new Mock<IPatternMatcher>();
            patternMatcherMock.Setup(r => r.TryReadNumber(It.IsAny<string>()))
                .Returns((false, 0));


            var sut = new NumberParser(patternMatcherMock.Object);

            //Act
            var result  = sut.ParseLine("some string");

            //Assert
            result.allMatched.Should().BeFalse();
            result.numbers.Count().Should().Be(0);
        }
    }
}
