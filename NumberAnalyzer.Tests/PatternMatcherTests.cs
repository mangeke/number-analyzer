using FluentAssertions;
using NumberAnalyzer.BusinessLogic.Concrete;
using Xunit;

namespace NumberAnalyzer.Tests
{
    public class PatternMatcherTests
    {
        [Theory]
        [InlineData("|", 1)]
        [InlineData("_|", 2)]
        [InlineData("/", 3)]
        [InlineData("|___|", 4)]
        [InlineData("|___", 5)]
        public void WhenTheCorrespondingPattern_ShouldReturnTrueWithRightNumber(string pattern, int number)
        {
            //Arrange
            var sut = new PatternMatcher();

            //Act
            var matchResult = sut.TryReadNumber(pattern);

            //Assert
            matchResult.success.Should().BeTrue();

            matchResult.number.Should().Be(number);
        }

        [Fact]
        public void WhenBadPattern_ShouldReturnFalseWithZero()
        {
            //Arrange
            var sut = new PatternMatcher();

            //Act
            var matchResult = sut.TryReadNumber("wrongpattern");

            //Assert
            matchResult.success.Should().BeFalse();

            matchResult.number.Should().Be(0);
        }
    }
}
