﻿using FluentAssertions;
using NumberAnalyzer.BusinessLogic.Concrete;
using Xunit;

namespace NumberAnalyzer.Tests
{
    public class DataValidatorTests
    {
        [Fact]
        public void WhenCorrectFormat_ShouldReturnTrue()
        {
            //Arrange
            var sut = new DataValidator();

            //Act
            var result = sut.IsValid("-\\/_| \t\r");

            //Assert
            result.Should().BeTrue();
        }

        [Fact]
        public void WhenBadFormat_ShouldReturnFalse()
        {
            //Arrange
            var sut = new DataValidator();

            //Act
            var result = sut.IsValid("badformat");

            //Assert
            result.Should().BeFalse();
        }

        [Fact]
        public void WhenEmptyString_ShouldReturnFalse()
        {
            //Arrange
            var sut = new DataValidator();

            //Act
            var result = sut.IsValid(string.Empty);

            //Assert
            result.Should().BeFalse();
        }
    }
}
