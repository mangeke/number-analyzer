﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NumberAnalyzer.BusinessLogic.Abstract;
using NumberAnalyzer.BusinessLogic.Concrete;
using NumberAnalyzer.BusinessLogic.Exceptions;
using System;
using System.Collections.Generic;
using System.IO;

namespace NumberAnalyzer.ConsoleApp
{
    class Program
    {
        private static IConfigurationRoot Configuration;

        static void Main(string[] args)
        {
            var builder = new ConfigurationBuilder();
            builder.AddUserSecrets<Program>();

            Configuration = builder.Build();

            var serviceProvider = new ServiceCollection()
                  .AddSingleton<INumberParser, NumberParser>()
                  .AddSingleton<IPatternMatcher, PatternMatcher>()
                  .AddSingleton<IDataValidator, DataValidator>()
                  .BuildServiceProvider();

            try
            {
                var filePath = Configuration["FilePath"];

                if (string.IsNullOrEmpty(filePath))
                {
                    throw new Exception("Wrong file path");
                }

                if (!File.Exists(filePath))
                {
                    throw new Exception("File does not exist");
                }

                var parser = serviceProvider.GetService<INumberParser>();
                var validator = serviceProvider.GetService<IDataValidator>();
                var numbers = new List<int>();

                using FileStream fs = File.Open(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                using BufferedStream bs = new BufferedStream(fs);
                using StreamReader sr = new StreamReader(bs);

                string line;
                var lineCount = 0;
                var nextLineToRead = 2;
                var unmatched = false;

                while ((line = sr.ReadLine()) != null)
                {
                    if (string.IsNullOrWhiteSpace(line))
                    {
                        continue;
                    }

                    lineCount++;

                    if (!validator.IsValid(line))
                    {
                        throw new DataFormatException("Wrong data format");
                    }

                    if (lineCount == nextLineToRead)
                    {
                        var parseResult = parser.ParseLine(line);
                        numbers.AddRange(parseResult.numbers);
                        if (!parseResult.allMatched)
                        {
                            unmatched = true;
                        }

                        nextLineToRead += 4;
                    }
                }

                Console.WriteLine(numbers.Count > 0 ? $"The numbers are: {string.Join(", ", numbers)}" :
                    "The file does not contain any numbers.");

                if(unmatched)
                {
                    Console.WriteLine("One or many numbers was not recognized. The pattern does not exist.");
                }

                if (lineCount % 4 != 0)
                {
                    throw new DataFormatException("Unexpected end of the file.");
                }
            }
            catch (Exception ex)
            {
                var msg = $"Error: {ex.Message}";
                Console.WriteLine(msg);
            }
        }
    }
}
