﻿using NumberAnalyzer.BusinessLogic.Abstract;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace NumberAnalyzer.BusinessLogic.Concrete
{
    public class NumberParser : INumberParser
    {
        private readonly IPatternMatcher _matcher;

        public NumberParser(IPatternMatcher matcher)
        {
            _matcher = matcher;
        }

        public (IEnumerable<int> numbers, bool allMatched) ParseLine(string inputStr)
        {
            var result = (numbers: new List<int>() , allMatched: true);

            var patterns = inputStr.Replace("\t", " ").Split(" ")
                .Where(r => !(Regex.IsMatch(r, @"^\s+$") || string.IsNullOrEmpty(r))).ToList();

            foreach (var pattern in patterns)
            {
                var matchResult = _matcher.TryReadNumber(pattern);

                if (matchResult.success)
                {
                    result.numbers.Add(matchResult.number);
                }
                else
                {
                    result.allMatched = false;
                }
            }

            return result;
        }
    }
}
