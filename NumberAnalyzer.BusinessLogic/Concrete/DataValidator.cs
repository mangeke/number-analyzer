﻿using NumberAnalyzer.BusinessLogic.Abstract;
using System.Text.RegularExpressions;

namespace NumberAnalyzer.BusinessLogic.Concrete
{
    public class DataValidator : IDataValidator
    {
        public bool IsValid(string str)
         => Regex.IsMatch(str, @"^[-\/|_\\\s\r\t]+$");
    }
}
