﻿using NumberAnalyzer.BusinessLogic.Abstract;

namespace NumberAnalyzer.BusinessLogic.Concrete
{
    public class PatternMatcher : IPatternMatcher
    {
        public (bool success, int number) TryReadNumber(string pattern)
        {
            (bool success, int number) result;
            switch (pattern)
            {
                case "|":
                    result.number = 1;
                    break;
                case "_|":
                    result.number = 2;
                    break;
                case "/":
                    result.number = 3;
                    break;
                case "|___|":
                    result.number = 4;
                    break;
                case "|___":
                    result.number = 5;
                    break;
                default:
                    result.number = 0;
                    result.success = false;
                    return result;
            }

            result.success = true;
            return result;
        }
    }
}
