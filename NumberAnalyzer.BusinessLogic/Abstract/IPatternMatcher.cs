﻿namespace NumberAnalyzer.BusinessLogic.Abstract
{
    public interface IPatternMatcher
    {
        (bool success, int number) TryReadNumber(string pattern);
    }
}
