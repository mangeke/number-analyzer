﻿namespace NumberAnalyzer.BusinessLogic.Abstract
{
    public interface IDataValidator
    {
        bool IsValid(string str);
    }
}
