﻿using System.Collections.Generic;

namespace NumberAnalyzer.BusinessLogic.Abstract
{
    public interface INumberParser
    {
        (IEnumerable<int> numbers, bool allMatched) ParseLine(string inputStr);
    }
}
